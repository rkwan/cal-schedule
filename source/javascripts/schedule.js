$(document).ready(function() {
    // column search from DataTables documentation examples
    // set up column search input elements
    $('#schedule tfoot th').each(function() {
        var title = $('#schedule thead th').eq($(this).index()).text();
        $(this).html('<input type="text" placeholder="' + title + '"/>');
    });

    // initialize table
    var table = $('#schedule').DataTable({
        "ajax": "javascripts/schedule.json",
        "buttons": [
            {
                "extend": "pageLength",
                "fade": 0,
            },
            {
                "extend": "colvis",
                "text": "Columns",
                "fade": 0
            },
            {
                "extend": "csv",
                "text": "CSV"
            }
        ],
        "deferRender": true,
        "dom": "Bfrtip",
        "language": {
            "buttons": {
                "pageLength": {
                    "-1": "All rows",
                    "_": "%d rows"
                }
            }
        },
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            ['10 rows', '25 rows', '50 rows', '100 rows', 'All rows (careful!)']
        ],
        "responsive": {
            "details": {
                "display": $.fn.dataTable.Responsive.display.childRowImmediate
            }
        }
    });

    // apply column search filter
    table.columns().every(function() {
        var column = this;
        $('input', this.footer()).on('keyup change', function() {
            if (column.search() !== this.value) {
                column.search(this.value, true, false).draw();
            }
        });
    });

    // arrow key bindings
    Mousetrap.bind(['s', 'd', 'j', 'l', 'right'], function(e) {
        table.page('next').draw('page');
        return false;
    });
    Mousetrap.bind(['w', 'a', 'k', 'h', 'left'], function(e) {
        table.page('previous').draw('page');
        return false;
    });
});
