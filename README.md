![Build Status](https://gitlab.com/rkwan/cal-schedule/badges/master/build.svg)

---

A GitLab Page for the UC Berkeley schedule of classes, scraped from
PDFs the registrar made available.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: ruby:2.1

cache:
  paths:
  - vendor

test:
  script:
  - apt-get update -yqqq
  - apt-get install -y nodejs
  - bundle install --path vendor
  - bundle exec middleman build
  except:
    - master

pages:
  script:
  - apt-get update -yqqq
  - apt-get install -y nodejs
  - bundle install --path vendor
  - bundle exec middleman build
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Middleman
1. Generate the website: `bundle exec middleman build`
1. Preview your project: `bundle exec middleman`
1. Add content

Read more at Middleman's [documentation][].

[ci]: https://about.gitlab.com/gitlab-ci/
[Middleman]: https://middlemanapp.com/
[install]: https://middlemanapp.com/basics/install/
[documentation]: https://middlemanapp.com/basics/install/

